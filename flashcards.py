import random

#Function to create and initialize the card list
def create_cards(cardList):
    #append all the values from 0 to 12 inside of list
    for i in range(0,13):
        cardList.append(i)

#Function to shuffle the card
def shuffle_cards(cardList):
    random.shuffle(cardList)

#create the question string and return
def question_string(number):
    return "12 * " + str(number) + " = "

#create the correct answer for specific question and return
def correct_answer(number):
    return 12 * number

#let the user guess the number within 4 attempts and return the score
def guess_number(number):
    question = question_string(number)  #call question_string function to create question string
    answer = correct_answer(number) #call correct_answer function to create correct answer for this question

    #loop 4 times since user has 4 attempts
    for j in range(1,5):
        userInput = int(input(question))    #ask user
        if userInput == answer: #if user get it right
            return 1
        else:   #if user get it wrong
            print("Not quite. Attempt Left:", 4-j)

    return 0

#main function
def main():

    print("-----------------------------")
    print("|  Welcome to Math Dungeon  |")
    print("|   We test your knowledge  |")
    print("|   Are you a Math Master?  |")
    print("-----------------------------")

    cardList = [] #initialize the card list
    create_cards(cardList)  #call create_cards function to fill in the cardList
    score = 0   #initialized total score

    shuffle_cards(cardList) #shuffle the card with shuffle_cards function

    for i in cardList:
        score += guess_number(i)

    if score == 13: #user gets 13 questions right
        score += guess_number(13)

    if score == 14: #user gets 14 questions right, then user is master
        print("You are a Math Master")

    print(score,"/ 13")


if __name__ == "__main__":
    main() #call out the main function to start the program